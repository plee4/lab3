//Paul Lee
//Kevin Shin
//Lab3 Part 2
//02/08/16


#ifndef TwoDayPackage_H
#define TwoDayPackage_H
#include "Package.h"
#include <iostream>
using namespace std;

class TwoDayPackage:public Package{
	public:
		TwoDayPackage();
		double calculateCost();
		
	private:
		double expedited;






};
#endif
