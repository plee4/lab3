all: main

main: main.o BankAccount.o BoatLoan.o CheckingAccount.o
	g++ main.o BankAccount.o BoatLoan.o CheckingAccount.o -o main


main.o: main.cpp
	g++ -c main.cpp
BankAccount.o: BankAccount.cpp
	g++ -c BankAccount.cpp
BoatLoan.o: BoatLoan.cpp
	g++ -c BoatLoan.cpp
CheckingAccount.o: CheckingAccount.cpp
	g++ -c CheckingAccount.cpp
	
clean:
	rm -f *.o main
