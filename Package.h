//Paul Lee
//Kevin Shin
//Lab4 Part2
//02/08/16

#ifndef Package_H
#define Package_H
#include <iostream>

using namespace std;

class Package{
	public:
		Package();
		double CalculateCost();
		
	private:
		string name;
		int zip;
		int weight;
		int cost;



};
