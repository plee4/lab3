//Paul Lee
//Kevin Shin
//Lab3 Part 2
//02/08/16

#ifndef OvernightPackage_H
#define OvernightPackage_H
#include "Package.h"

using namespace std;

class OvernightPackage:public Package{
    public:
        double extraCostPerOunce();
        double calculateCost();






};
